package main

import "errors"

// Service interface
type Service interface {
	SayHello(name string) (string, error)
}

type service struct{}

func (s *service) SayHello(name string) (string, error) {
	if len(name) == 0 {
		return "", errors.New("name len > 0 is a required param")
	}

	return "Hello " + name, nil
}
