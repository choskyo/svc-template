package main

import "github.com/gin-gonic/gin"

func main() {
	svc := &service{}

	router := gin.Default()
	router.GET("/hello/:name", makeSayHelloEndpoint(svc))

	router.Run(":8080")
}
