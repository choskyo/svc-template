# svc-template

Template I've been using as a base when writing go services, uses gin because simple requirements + less boilerplate.

#### After cloning:

- Update scripts/dev.sh to have service-specific name, ports etc
- Update port in main.go
- `$ go mod vendor`
- `$ ./scripts/dev.sh`
