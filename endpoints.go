package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func makeSayHelloEndpoint(s Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		name := c.Param("name")

		resMsg, err := s.SayHello(name)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"err": err,
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"response": resMsg,
		})
	}
}
