#!/bin/sh
docker build -t svc-template .
docker rm --force template
docker run -d -p 8080:8080 --name template svc-template:latest
docker network connect project-network template