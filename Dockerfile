FROM golang:alpine

ENV GO111MODULE=on

WORKDIR /app

COPY . .

RUN go build -mod vendor

EXPOSE 8080

ENTRYPOINT ["/app/svc-template"]